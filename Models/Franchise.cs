﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models
{
    public class Franchise
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name{ get; set; }
        [MaxLength(300)]
        public string Description { get; set; }
        // Relationships
        public ICollection<Movie> Movies { get; set; }
    }
}
