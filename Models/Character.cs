﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models
{
    public class Character
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        [DataType(DataType.ImageUrl)]
        public string URL { get; set; }

        //Relationship
        public ICollection<Movie> Movies { get; set; }
    }
}
