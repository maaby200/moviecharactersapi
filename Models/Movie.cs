﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        [MaxLength(100)]
        public string Genres { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        public int ReleaseYear { get; set; }
        [DataType(DataType.ImageUrl)]
        public string Trailer { get; set; }
        [DataType(DataType.ImageUrl)]
        public string Picture { get; set; }

        public ICollection<Character> Characters { get; set; }
        public Franchise Franchise { get; set; }
        public int FranchiseId { get; set; }
    }
}
